# Code Golf

The repository for our Code Golf competition.

## Rules

1. Everything needs to be written by you, a human! No copy-pasting allowed!
2. External packages are added to the character count.
3. Getting data from websites is not allowed.

## Getting Started

First, you need to clone this repository to your local machine, or edit it
in the browser.

### Editing in browser

1.
First, create a new branch.
You can use the '+' button next to "codegolf /" to create one.

2.
Name this branch as your nickname of choice.
Make sure you remember this nickname, as this will be your submission!
Press 'Create Branch' to create.

3.
You can now create your source files in the browser.
Press the '+' button again, and click 'New File'.

4.
Once you're done with your file, click 'Commit changes'.
That's you done!

You can keep repeating steps 3 and 4 as many times as you like, if you can
think of a shorter solution!


### Cloning Locally

1.
Cloning locally can be done by running:
`git clone https://gitlab.com/aucs/codegolf`
or
`git clone ssh://git@gitlab.com/aucs/codegolf`

2.
Then, create a new branch, and name it with your nickname.
Make sure you remember this nickname, as this will be your submission!
`git checkout -b <nickname here>`

3.
You can now start writing code!
Once you want to submit, you can simply add your files to the commit:
`git add --all`

4.
And then push the changes to the server!
`git push origin <nickname here>`

You can keep repeating steps 3 and 4 as many times as you like, if you can
think of a shorter solution! Only the solution that you `push`ed last counts!

Happy hacking!

### Did you have fun?

If you enjoyed the hack day and want to come to a future event then [subscribe to our brand new mailing list](http://eepurl.com/ge0QPf) 